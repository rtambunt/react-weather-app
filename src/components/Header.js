import React, { useEffect, useState } from "react";
import axios from "axios";
import { ReactComponent as Sun } from "../icons/sun.svg";
import { ReactComponent as Drizzle } from "../icons/cloud-drizzle.svg";
import { ReactComponent as Cloud } from "../icons/cloud.svg";
import { ReactComponent as Rain } from "../icons/cloud-rain.svg";
import { ReactComponent as Snow } from "../icons/cloud-snow.svg";
import { ReactComponent as Lightning } from "../icons/cloud-lightning.svg";

export default function Header() {
  const [currentWeather, setCurrentWeather] = useState({});
  const [currentTemp, setCurrentTemp] = useState("");
  const [iconCode, setIconCode] = useState("");

  const fetchTemp = () => {
    axios
      .get(
        "https://api.open-meteo.com/v1/forecast?latitude=37.77&longitude=-122.42&hourly=temperature_2m,apparent_temperature,precipitation,weathercode,windspeed_10m&daily=weathercode,temperature_2m_max,temperature_2m_min,apparent_temperature_max,apparent_temperature_min,precipitation_sum&current_weather=true&temperature_unit=fahrenheit&windspeed_unit=mph&precipitation_unit=inch&timeformat=unixtime&timezone=America%2FLos_Angeles"
      )
      .then((res) => {
        setCurrentTemp(res.data.current_weather.temperature);
        setIconCode(res.data.current_weather.weathercode);
      });
  };

  useEffect(() => {
    fetchTemp();
  }, []);

  const weatherIcon = (function () {
    if (iconCode === 0) {
      return <Sun />;
    } else if (iconCode >= 1 && iconCode <= 3) {
      return <Cloud />;
    } else if (
      (iconCode >= 50 && iconCode <= 60) ||
      (iconCode >= 80 && iconCode <= 82)
    ) {
      return <Drizzle />;
    } else if (iconCode > 60 && iconCode <= 70) {
      return <Rain />;
    } else if (
      (iconCode > 70 && iconCode < 80) ||
      iconCode === 85 ||
      iconCode === 86
    ) {
      return <Snow />;
    } else if (iconCode >= 95 && iconCode < 100) {
      return <Lightning />;
    } else {
      return <Cloud />;
    }
  })();

  return (
    <header className="header">
      <div className="header__left">
        <div className="svg-large">{weatherIcon}</div>

        <div className="header__current-temp">{currentTemp}°</div>
      </div>
      <div className="header__right">
        <div className="header__info-group">
          <div className="label">High</div>
          <div>32°</div>
        </div>

        <div className="header__info-group">
          <div className="label">Fl High</div>
          <div>32°</div>
        </div>

        <div className="header__info-group">
          <div className="label">Wind</div>
          <div>32°</div>
        </div>

        <div className="header__info-group">
          <div className="label">Low</div>
          <div>32°</div>
        </div>

        <div className="header__info-group">
          <div className="label">Fl Low</div>
          <div>32°</div>
        </div>

        <div className="header__info-group">
          <div className="label">Precip</div>
          <div>32°</div>
        </div>
      </div>
    </header>
  );
}
