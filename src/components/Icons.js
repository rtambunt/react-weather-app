import React from "react";
import sun from "../icons/sun.svg";
import drizzle from "../icons/cloud-drizzle.svg";
import lightning from "../icons/cloud-lightning.svg";
import snow from "../icons/cloud-snow.svg";
import cloud from "../icons/cloud.svg";

export default function Icons(props) {
  return <div>{sun}</div>;
}
